class MoveFirstNameOfUsers < ZgcManager.migration_class
  def change
    add_column ZgcCms::User.table_name, :first_name, :string unless column_exists?(ZgcCms::User.table_name, :first_name)
    add_column ZgcCms::User.table_name, :last_name, :string unless column_exists?(ZgcCms::User.table_name, :last_name)
    ZgcCms::User.all.each do |u|
      u.update_columns(first_name: u.get_meta('first_name'), last_name: u.get_meta('last_name')) if u.get_meta('first_name').present?
    end
  end
end
