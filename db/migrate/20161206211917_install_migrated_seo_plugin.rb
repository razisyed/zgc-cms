class InstallMigratedSeoPlugin < ZgcManager.migration_class
  # install seo plugin without calling hooks (seo logic moved to separated plugin)
  def change
    ZgcCms::Site.all.each do |s|
      s.plugins.where(slug: 'zgc_meta_tag').first_or_create!(term_group: 1)
    end
  end
end
