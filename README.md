# ZERO GRAVITY CMS

[Website](http://zerogravityco.com/)

## About

Zero Gravity Co CMS is a dynamic and advanced content management system based on Ruby on Rails that adapts to your needs. This CMS is an alternative to Wordpress for Ruby on Rails developers to manage advanced content easier.

Zero Gravity Co CMS is a flexible manager where you can build your custom content structure without coding anything by custom fields and custom contents type.

![](screenshot.png)

## With Zero Gravity Co you can do:
* Integrate into existing Rails projects
* Multiples sites in the same installation
* Multi-language sites
* Design and create the architecture of your project without programming by dynamic contents and fields
* Extend or customize the functionality using plugins
* Manage your content visualization using themes
* Easier administration. Zero Gravity Co CMS permits you to adapt the CMS to all your needs and not you adapt to the CMS. You can create your custom architecture with any custom attributes that you need for all content types.

## Some features
* Zero Gravity Co CMS is FREE and Open source
* Shortcodes
* Widgets
* Drag and Drop / Sortable / Multi level menus
* Templates/Layouts for pages
* Advanced User roles
* File Uploads with built in Local and Amazon S3 support
* Easy migration from Wordpress
* Security
  - Remote code execution
  - SQL injections
  - Advanced sessions security
  - Cross Site Scripting
  - Control of abusive requests
  - Cross-Site Request Forgery
* Site Speed
  Zero Gravity Co CMS include a lot of cache strategies to optimize the site access velocity:
    - Cache contents
    - Cache queries
    - Manifests (compress and join asset files)
    - Customize your content visualization for Desktop, Mobile and Tablet
* SEO & HTML5
  - Sitemap generations
  - Seo configuration
  - Seo for social media
  - All generated content is compatible with HTML5 and Bootstrap 3


## Requirements
* Rails 4.2 or 5+
* PostgreSQL, MySQL 5+ or SQlite
* Ruby 2.2+
* Imagemagick

## Installation
* Install Ruby on Rails
* Create your rails project

  ```
  rails new my_project
  ```
* Add the gem in your Gemfile 

  ```
 # gem 'draper', '~> 3' # for Rails 5+
  ```

* Install required Gem and dependencies

  ```
  bundle install
  ```
* Zero Gravity Co CMS Installation

  ```
  rails generate zgc_cms:install
  ```
* (Optional) Before continue you can configure your CMS settings in (my_app/config/system.json), [here](config/system.json) you can see the full settings.
* Create database structure
  ```
  rake zgc_cms:generate_migrations
  # before running migrations you can customize copied migration files
  rake db:migrate
  ```

* Start your server

  ```
  rails server
  ```

## Author
Zero Gravity Co 
