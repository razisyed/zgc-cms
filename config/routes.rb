Rails.application.routes.draw do
  scope PluginRoutes.system_info["relative_url_root"], as: "zgc" do
    # root "application#index"
    default_url_options PluginRoutes.default_url_options

    # public
    get 'error', as: "error", to: 'zgc_cms/zgc#render_error'
    get 'captcha', as: "captcha", to: 'zgc_cms/zgc#captcha'
    eval(PluginRoutes.load("main"))
  end
end
