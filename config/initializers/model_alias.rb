module Zgc
end
Rails.application.config.to_prepare do
  if PluginRoutes.static_system_info['user_model'].present?
    ZgcCms::User = PluginRoutes.static_system_info['user_model'].constantize
    ZgcCms::User.class_eval do
      include ZgcCms::UserMethods
    end
  end
  Zgc::User = ZgcCms::User unless defined? Zgc::User
end
Zgc::Site = ZgcCms::Site
Zgc::Post = ZgcCms::Post
Zgc::Category = ZgcCms::Category
Zgc::PostTag = ZgcCms::PostTag
Zgc::PostType = ZgcCms::PostType
Zgc::TermTaxonomy = ZgcCms::TermTaxonomy
Zgc::TermRelationship = ZgcCms::TermRelationship