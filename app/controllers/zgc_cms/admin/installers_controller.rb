class ZgcCms::Admin::InstallersController < ZgcCms::ZgcController
  skip_before_action :zgc_site_check_existence
  skip_before_action :zgc_before_actions
  skip_after_action :zgc_after_actions
  before_action :installer_verification, except: :welcome
  layout "zgc_cms/login"

  def index
    @site ||= ZgcCms::Site.new
    @site.slug = request.original_url.to_s.parse_domain
    render "form"
  end

  def save
    @site = ZgcCms::Site.new(params[:site].permit(:slug, :name )).decorate
    if @site.save
      site_after_install(@site, params[:theme])
      flash[:notice] = t('zgc_cms.admin.sites.message.created')
      redirect_to action: :welcome
    else
      index
    end
  end

  def welcome

  end

  def installer_verification
    redirect_to zgc_root_url unless ZgcCms::Site.count == 0
  end
end
