class ZgcCms::Admin::Settings::SitesController < ZgcCms::Admin::SettingsController
  before_action :set_site, only: [:show, :edit, :update, :destroy]
  before_action :check_shared_status
  add_breadcrumb I18n.t("zgc_cms.admin.sidebar.sites"), :zgc_admin_settings_sites_path

  def index
    @sites = ZgcCms::Site.all.order(:term_group)
    @sites = @sites.paginate(:page => params[:page], :per_page => current_site.admin_per_page)
    r = { sites: @sites, render: "index" }
    hooks_run("list_site", r)
    render r[:render]
  end

  def show
  end

  def edit
    add_breadcrumb I18n.t("zgc_cms.admin.button.edit")
    render 'form'
  end

  def update
    tmp = @site.slug
    if @site.update(params.require(:site).permit!)
      save_metas(@site)
      flash[:notice] = t('zgc_cms.admin.sites.message.updated')
      if @site.id == Zgc::Site.main_site.id && tmp != @site.slug
        redirect_to @site.the_admin_url
      else
        redirect_to action: :index
      end
    else
      edit
    end
  end

  def new
    add_breadcrumb I18n.t("zgc_cms.admin.button.new")
    @site ||= ZgcCms::Site.new.decorate
    render 'form'
  end

  def create
    site_data = params.require(:site).permit!
    @site = ZgcCms::Site.new(site_data)
    if @site.save
      save_metas(@site)
      site_after_install(@site, @site.get_theme_slug)
      flash[:notice] = t('zgc_cms.admin.sites.message.created')
      redirect_to action: :index
    else
      new
    end
  end

  def destroy
    flash[:notice] = t('zgc_cms.admin.sites.message.deleted') if @site.destroy
    redirect_to action: :index
  end

  private

  def save_metas(site)
    if params[:metas].present?
      params[:metas].each do |meta, val|
        site.set_meta(meta, val)
      end
    end
  end

  def set_site
    begin
      @site = ZgcCms::Site.find_by_id(params[:id]).decorate
    rescue
      flash[:error] = t('zgc_cms.admin.sites.message.error')
      redirect_to zgc_admin_path
    end
  end

  # check if the system.config manage shared users
  def check_shared_status
    unless current_site.manage_sites?
      flash[:error] = t('zgc_cms.admin.sites.message.unauthorized')
      redirect_to zgc_admin_path
    end
  end
end
