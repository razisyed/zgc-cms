class ZgcCms::UniqValidator < ActiveModel::Validator
  def validate(record)
    unless record.skip_slug_validation?
      record.errors[:base] << "#{I18n.t('zgc_cms.admin.post.message.requires_different_slug')}" if ZgcCms::TermTaxonomy.where(slug: record.slug).where.not(id: record.id).where("#{ZgcCms::TermTaxonomy.table_name}.taxonomy" => record.taxonomy).where("#{ZgcCms::TermTaxonomy.table_name}.parent_id" => record.parent_id).size > 0
    end
  end
end
class ZgcCms::TermTaxonomy < ActiveRecord::Base
  include ZgcCms::Metas
  include ZgcCms::CustomFieldsRead
  self.table_name = "#{PluginRoutes.static_system_info["db_prefix"]}term_taxonomy"
  # attr_accessible :taxonomy, :description, :parent_id, :count, :name, :slug, :term_group, :status, :term_order, :user_id
  # attr_accessible :data_options
  # attr_accessible :data_metas

  # callbacks
  before_validation :before_validating
  before_destroy :destroy_dependencies

  # validates
  validates :name, :taxonomy, presence: true
  validates_with ZgcCms::UniqValidator

  # relations
  has_many :term_relationships, :class_name => "ZgcCms::TermRelationship", :foreign_key => :term_taxonomy_id, dependent: :destroy
  # has_many :posts, foreign_key: :objectid, through: :term_relationships, :source => :objects
  belongs_to :parent, class_name: "ZgcCms::TermTaxonomy", foreign_key: :parent_id
  belongs_to :owner, class_name: ZgcManager.get_user_class_name, foreign_key: :user_id

  # return all children taxonomy
  # sample: sub categories of a category
  def children
    ZgcCms::TermTaxonomy.where("#{ZgcCms::TermTaxonomy.table_name}.parent_id = ?", self.id)
  end

  # return all menu items in which this taxonomy was assigned
  def in_nav_menu_items
    ZgcCms::NavMenuItem.where(url: self.id, kind: self.taxonomy)
  end

  # permit to skip slug validations for children models, like menu items
  def skip_slug_validation?
    false
  end

  private
  # callback before validating
  def before_validating
    slug = self.slug
    slug = self.name if slug.blank?
    self.name = slug unless self.name.present?
    self.slug = slug.to_s.parameterize.try(:downcase)
  end

  # destroy all dependencies
  # unassign all items from menus
  def destroy_dependencies
    in_nav_menu_items.destroy_all
  end

end
