// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets) for details
// about supported directives.
//
//= require jquery
//= require zgc_cms/admin/_data
//= require zgc_cms/bootstrap.min
//= require zgc_cms/admin/_jquery-ui.min
//= require zgc_cms/admin/_underscore
//= require zgc_cms/admin/jquery.validate

// date picker
//= require zgc_cms/admin/momentjs/_moment
//= require zgc_cms/admin/_bootstrap-datepicker

//= require jquery_ujs
//= require zgc_cms/admin/_bootstrap-select

//= require zgc_cms/admin/_jquery.slugify
//= require zgc_cms/admin/_translator
//= require zgc_cms/admin/_i18n
//= require zgc_cms/admin/_custom_fields

//= require zgc_cms/admin/_libraries
//= require zgc_cms/admin/_actions
//= require zgc_cms/admin/introjs/_intro.min

// uploader
//= require zgc_cms/admin/uploader/uploader_manifest

//= require zgc_cms/admin/tageditor/_jquery.caret.min
//= require zgc_cms/admin/tageditor/_jquery.tag-editor

//= require zgc_cms/admin/lte/app

// tinymce
//= require tinymce-jquery
//= require zgc_cms/admin/tinymce/plugins/filemanager/plugin.min

// post
//= require zgc_cms/admin/_jquery.tagsinput.min
//= require zgc_cms/admin/_post

//= require zgc_cms/admin/_posttype

// others
//= require zgc_cms/admin/_user_profile
