module ZgcCms::ContentHelper
  # initialize content variables
  def zgc_content_init
    @_before_content = []
    @_after_content = []
  end

  # prepend content for admin or frontend (after <body>)
  # sample: zgc_content_prepend(<div>my prepend content</div>)
  def zgc_content_prepend(content)
    @_before_content << content
  end

  # append content for admin or frontend (before </body>)
  # sample: zgc_content_prepend(<div>my after content</div>)
  def zgc_content_append(content)
    @_after_content << content
  end

  # draw all before contents registered by zgc_content_prepend
  def zgc_content_before_draw
    @_before_content.join("") rescue ""
  end

  # draw all after contents registered by zgc_content_append
  def zgc_content_after_draw
    @_after_content.join("") rescue ""
  end
end
