module ZgcCms::Admin::ApplicationHelper
  # include ZgcCms::Admin::ApiHelper
  include ZgcCms::Admin::MenusHelper
  include ZgcCms::Admin::PostTypeHelper
  include ZgcCms::Admin::CategoryHelper
  include ZgcCms::Admin::CustomFieldsHelper

  # render pagination for current items
  # items is a will pagination object
  # sample: <%= raw zgc_do_pagination(@posts) %>
  def zgc_do_pagination(items, *will_paginate_options)
    will_paginate_options = will_paginate_options.extract_options!
    custom_class = will_paginate_options[:panel_class]
    will_paginate_options.delete(:panel_class)
    "<div class='row #{custom_class} pagination_panel zgc_ajax_request'>
        <div class='col-md-10'>
          #{will_paginate(items, will_paginate_options) rescue '' }
        </div>
        <div class='col-md-2 text-right total-items'>
          <strong>#{I18n.t('zgc_cms.admin.table.total', default: 'Total')}: #{items.total_entries rescue items.count} </strong>
        </div>
    </div>"
  end

  # return the locale for frontend translations initialized in admin controller
  # used by models like posts, categories, ..., sample: my_post.the_url
  # fix for https://github.com/owen2345/camaleon-cms/issues/233#issuecomment-215385432
  def zgc_get_i18n_frontend
    @zgc_i18n_frontend
  end

  # print code with auto copy
  def zgc_shortcode_print(code)
    "<input onmousedown=\"this.clicked = 1;\" readonly onfocus=\"if (!this.clicked) this.select(); else this.clicked = 2;\" onclick=\"if (this.clicked == 2) this.select(); this.clicked = 0;\" class='code_style' tabindex='-1' value=\"#{code}\">"
  end
end
